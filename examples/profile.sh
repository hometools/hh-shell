#!/bin/bash

. $HOME/.config/shell/lib.sh


# Home configuration
# -----------------------------

# Destination of bash pieces
env_vars_add LOADER_DST=$HOME/.local/share/shell

# Custom bin linker
AUTOBIN_DST=$HOME/.local/autobin
add_path $AUTOBIN_DST

# Python management
env_vars_add PYTHONUSERBASE=${HOME}/opt/python/home
add_path ${PYTHONUSERBASE}/bin

# Go management
env_vars_add GOPATH=${HOME}/opt/go
add_path ${GOPATH}/bin

# Finally
env_vars_save
load_parts

