#!/bin/bash

. $HOME/.config/shell/profile.sh full


# Install packages
# -----------------------------

install_pkg ()
{
  
  # Shell utils
  pkg_add vim-enhanced
  pkg_add git
  pkg_add htop
  pkg_add iftop
  pkg_add expect
  pkg_add strace
  
  pkg_install
  
   # Desktop
  pkg_add terminator
  pkg_add keepassxc
  pkg_add Zim
  pkg_add syncthing-gtk
  pkg_add chromium
  pkg_add slack
  pkg_add compat-ffmpeg28
  pkg_add steam
  
  # Sync tools
  pkg_add syncthing
  pkg_add unison240-gtk
  pkg_add unison251-gtk
  pkg_add synergy
  
  # Devops tools
  #pkg_add direnv
  
  pkg_install
  
  # Python related
  pkg_add python3-pyyaml
  pkg_add python37 python34
  pkg_add python2-devel
  
  pkg_add python2-dnf
  pkg_add libselinux-python2
  pkg_add python3-virtualenv
  
  pkg_add pipsi
  pkg_add pipenv
  
  # Misc
  pkg_add libbsd
  pkg_add golang
  
  # Install packages
  pkg_install

}

# Install shell helpers
# -----------------------------
install_shell ()
{
  if is_hostname mtl-bj714.ubisoft.org; then
    echo "My action !!!!"
  fi

  # Todo list:
  # Deploy secrets
  # Deply generic config
  # Deploy ubisoft config
  # Use vcsh ?

  add_loader 90-aliases "cat $DIR/90-myaliases.sh"
}

# Custom home programs
# -----------------------------

install_tooling ()
{
  # Programs from repos
  GITHUB=$HOME/opt/git/github.com
  git_repo $GITHUB/mrjk/vcsh https://github.com/mrjk/vcsh/
  git_repo $GITHUB/mrjk/yadm https://github.com/mrjk/yadm/
  
  link_bin_in_path vcsh $GITHUB/mrjk/vcsh/vcsh
  link_bin_in_path yadm $GITHUB/mrjk/yadm/yadm
  
  # Ubi scripts
  GITLAB_NCSA=$HOME/opt/git/gitlab-ncsa.ubisoft.org
  UBI_SCRIPTS=$GITLAB_NCSA/rcordier/ubi_scripts
  git_repo $UBI_SCRIPTS git@gitlab-ncsa.ubisoft.org:rcordier/ubi_scripts.git
  
  link_bin_in_path json2yaml $UBI_SCRIPTS/json2yaml
  link_bin_in_path ssh_unsecure $UBI_SCRIPTS/ssh_unsecure
  link_bin_in_path ubi_helper $UBI_SCRIPTS/ubi_helper
  link_bin_in_path ubi_openstack $UBI_SCRIPTS/ubi_openstack
  link_bin_in_path xdo_paste $UBI_SCRIPTS/xdo_paste
  link_bin_in_path yaml2json $UBI_SCRIPTS/yaml2json
  
  # Go programs
  go_install direnv github.com/direnv/direnv
  add_loader 50-direnv "direnv hook ${SHELL##*/}"
  
  # Completions
  add_completion openstack "openstack complete"
  
  
  # Other tools to install
  PIP_DB_PATH=$PYTHONUSERBASE/db
  pip_install_req .config/python/openstack-queens.req
  pip_install_req .config/python/ansible27.req

}


# Project configuration
# -----------------------------

install_prj ()
{
  UBI_PROD=$HOME/prj/ubi/prod
  git_repo $UBI_PROD/ansible-ilo-management git@gitlab-ncsa.ubisoft.org:iaas/ansible-ilo-management.git
  git_repo $UBI_PROD/ansible-swift git@gitlab-ncsa.ubisoft.org:iaas/ansible-swift.git
  git_repo $UBI_PROD/cloud-doc git@gitlab-ncsa.ubisoft.org:iaas/teams-documentation/cloud-doc.git
  git_repo $UBI_PROD/iaas-alerts git@gitlab-ncsa.ubisoft.org:iaas/iaas-alerts
  git_repo $UBI_PROD/machines-inventory git@gitlab-ncsa.ubisoft.org:iaas/machines-inventory.git
  git_repo $UBI_PROD/openstack-config git@gitlab-ncsa.ubisoft.org:rcordier/openstack-config.git
  
  
  UBI_DEV=$HOME/prj/ubi/dev
  git_repo $UBI_DEV/stackmailer git@gitlab-ncsa.ubisoft.org:iaas/StackMailer.git
  
  UBI_IAAS=$HOME/prj/ubi/dev
  git_repo $UBI_IAAS/client-communication git@gitlab-ncsa.ubisoft.org:iaas/tools/client-communication.git
  git_repo $UBI_IAAS/cloud-samples git@gitlab-ncsa.ubisoft.org:iaas/cloud-samples.git
  git_repo $UBI_IAAS/iaas-documentation-bootstrap git@gitlab-ncsa.ubisoft.org:iaas/teams-documentation/iaas-documentation-bootstrap.git
  git_repo $UBI_IAAS/iaas-toolbox git@gitlab-ncsa.ubisoft.org:iaas/tools/iaas-toolbox.git
  git_repo $UBI_IAAS/linux-desktop-doc git@gitlab-ncsa.ubisoft.org:iaas/linux-desktop-doc.git
  git_repo $UBI_IAAS/quay-sync git@gitlab-ncsa.ubisoft.org:iaas/docker/quay-sync.git
}

ACTION=$1

case $ACTION in 

	# Selective installs
	pkg) install_pkg;;
	tooling) install_tooling;;
	prj) install_prj;;

	# All install
	*)
		install_pkg
		install_tooling
		install_prj
	;;
esac







