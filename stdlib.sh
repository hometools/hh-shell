#!/bin/bash

# Other examples: https://github.com/direnv/direnv/blob/master/stdlib.sh


# Profile helpers
# -----------------------------

LOADER_DST="${LOADER_DST:-$HOME/.local/share/shell/}"
ENV_PREFIX="${ENV_PREFIX:-10-}"

env_vars_add ()
{
  local string=$@

  if [ -z "$ENV_TMP_FILE" ]; then
    ENV_TMP_FILE=$(mktemp /tmp/env_vars.XXXXXX)
    > $ENV_TMP_FILE
  fi
  eval "export $string"
  echo "$string" >> $ENV_TMP_FILE
}

env_vars_save ()
{
  if [ -z "$ENV_TMP_FILE" ]; then
    >&2 echo "ERROR: No env file to apply :("
  fi
  cp $ENV_TMP_FILE $LOADER_DST/${ENV_PREFIX}env.load.sh
}

add_path ()
{
  local path=$(echo $1)
  
  # Load path
  if [[ ":$PATH:" != *":$path:"* ]]; then
    PATH="$path:${PATH}"
  #  >&2 echo "INFO: directory '$path' has been added to PATH "
  #else
  #  >&2 echo "INFO: directory '$path' is already in PATH "
  fi
}


load_parts ()
{
  for i in $LOADER_DST/*.sh ; do
    >&2 echo "INFO: Loading $i ..."
    . $i
  done
}


is_hostname ()
{
  [ "$HOSTNAME" == "${1-}" ]
}

# Std helpers
# -----------------------------
if [ "${1-}" == "full" ]; then
  COMPL_PREFIX="${COMPL_PREFIX:-60-}"
  AUTOBIN_DST="${AUTOBIN_DST:-}"
  PIP_DB_PATH="${PYTHONUSERBASE:-$HOME/.local/share/pip}/db"

  git_repo ()
  {
    local path=$1
    local url=$2
  
    if [ ! -d "$path" ]; then
      >&2 echo "INFO: Clonning repository for: $path ..."
      echo git clone $url $path
      git clone $url $path
    else
      >&2 echo "INFO: Repository is present: $path ..."

      ( cd $path && git pull --rebase || echo "WARN: Could not pull repo $url")
    fi
  }
  
  go_install ()
  {
    local name=$1
    local url=$2
  
    if ! command -v $name &>/dev/null; then
      >&2 echo "INFO: Installing go package: $name ..."
      go get $url
    fi
  }
  
  
  add_loader ()
  {
    local name=$1
    local cmd=$2
  
    mkdir -p $LOADER_DST
    if [ ! -f "$LOADER_DST/$name.load.sh" ]; then
      >&2 echo "INFO: Creating loader for: $name ..."
      $cmd > "$LOADER_DST/$name.load.sh"
    fi
  }
  
  add_completion ()
  {
    local name=$1
    local cmd=$2
    local dst="$LOADER_DST/$COMPL_PREFIX$name.compl.sh"
  
    mkdir -p $LOADER_DST
    #if [[ ! -f "$dst" || -s "$dst" ]]; then
    if [[ ! -s "$dst" || ! -f "$dst" ]]; then
      >&2 echo "INFO: Creating completion for: $name ..."
      $cmd > "$dst"
    fi
  }
  
  
  link_bin_in_path ()
  {
    local name=$1
    local dst=$2
    mkdir -p $AUTOBIN_DST
    #echo ln -sR $dst $AUTOBIN_DST/$name

    (
      cd $HOME
      if [ ! -f "$AUTOBIN_DST/$name" ]; then
        >&2 echo "INFO: Creating bin link for: $name"
        ln -sr $dst $AUTOBIN_DST/$name
      else
        >&2 echo "INFO: Bin link present for: $name"
      fi
    )
  }
  
  pip_mark_pkg ()
  {
    local action=$1
    local name=$2
  
    if [ "$action" == "installed" ]; then
      mkdir -p $PIP_DB_PATH
      touch $PIP_DB_PATH/$name
    else
      rm $PIP_DB_PATH/$name 2>/dev/null
    fi
  }
  
  pip_install_req ()
  {
    local name=$1
    local short=${name##*/}
    if [ ! -f "$PIP_DB_PATH/$short" ]; then
      pip3 install --user -r $HOME/$name
      pip_mark_pkg installed ${short}
    fi
  }
  
  pip27_install_req ()
  {
    local name=$1
    local short=${name##*/}
    if [ ! -f "$PIP_DB_PATH/$short" ]; then
      pip2 install --user -r $HOME/$name
      pip_mark_pkg installed $short
    fi
  }
  
  
  
  pkg_add ()
  {
    local string=$@
  
    if [ -z "$PKG_TMP_FILE" ]; then
      PKG_TMP_FILE=$(mktemp /tmp/pkg_list.XXXXXX)
      > $PKG_TMP_FILE
    fi
    echo "$string" >> $PKG_TMP_FILE
  }
  
  pkg_install ()
  {
    if [ -z "$PKG_TMP_FILE" ]; then
      >&2 echo "ERROR: No packages to install :("
    fi
  
    rpm -qa --qf "%{NAME}\n" > /tmp/pkg_list
    local pkg=$(cat $PKG_TMP_FILE | xargs -n 1 | grep -v -f /tmp/pkg_list)
  
    if [ ! -z "$pkg" ]; then
      >&2 echo "INFO: Installing: $pkg"
      sudo dnf install $pkg && rm $PKG_TMP_FILE && rm /tmp/pkg_list
    else
      >&2 echo "INFO: All packages are present"
    fi
  }

  vcsh_present ()
  {
    local remote_name=$1
    local repo_name=${remote_name##*/}

    local VCSH_REPO_D="${XDG_CONFIG_HOME:-$HOME/.config}/vcsh/repo.d"
    local dst=$VCSH_REPO_D/$repo_name

    if ! [[ -d $dst || -s $dst ]]; then
      vcsh clone $remote_name
    else
      >&2 echo "INFO: Vcsh repo $repo_name is already present"
      vcsh ${repo_name%%.git} pull --rebase || >&2 echo "WARN: Could not pull repo $repo_name"
    fi
  }
fi
