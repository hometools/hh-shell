
# hh-shell

## Quickstart

### Method 1:

```
curl $url
```

## Other parts

### hh-install

Provide an easy way to de ployhome folders.

Usage:
```
$ hh-install --help
INFO: Config loaded: /home/jez/.config/shell/install.sh
  Usage: ./hh-install list
         ./hh-install install [all|PKG]

  Commands:
    list        : List pack
    install     : Install a specific pack or all by default

```
